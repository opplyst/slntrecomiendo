﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tr.WebProject.Controllers
{
    public class ListarController : Controller
    {
        // GET: Listar
        public ActionResult Index()
        {
            var generic = new Library.Generic();
            var datos = generic.GetNotify();
            return View(datos);
        }
    }
}