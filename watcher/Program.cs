﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace watcher
{
	class MainClass
	{
		 static void Main(string[] args)
		{
            var aTimer = new System.Timers.Timer();
            aTimer.Elapsed += timer_Tick;
            aTimer.Interval = 60000;
            aTimer.Enabled = true;
            Console.WriteLine("Press \'q\' to quit the Excecution.");
            while (Console.Read() != 'q') ;
            
		}

        public static void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                RunAsync().Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
            }
        }

        /// <summary>
        /// Tarea Asincrona que lanza los eventos necesarios
        /// </summary>
        /// <returns></returns>
        public static async Task RunAsync()
        {
            Mensajes();
        }

        private static void Mensajes()
        {
            var n = new WatcherLogic();

            n.Notificar(new Tr.Library.Notify3Days(), " true ");

            n.Notificar(new Tr.Library.Notify6Days(), " true ");

            n.Notificar(new Tr.Library.NotifyLog(), " true ");
            
        }
    }
}
