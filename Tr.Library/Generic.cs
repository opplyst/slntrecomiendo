﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tr.Entities;

namespace Tr.Library
{
    public class Generic
    {
        public List<Entities.TblLogNotify> GetNotify()
        {
            var DataAccessLayer = new Tr.DataAccess.Getter();
            return DataAccessLayer.GetAllNotification();
        }
    }
}
