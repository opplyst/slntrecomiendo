﻿using System;
namespace Tr.Library
{
	public interface INotification
	{
		void NotifyLaunch();

        void NotifyAPI(Entities.TblTrabajadore nTrabajadores);
	}
}

