﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Tr.Library
{
    public class RequestManager
    {
        public string _lastResponse { protected set; get; }

        CookieContainer _cookies = new CookieContainer();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nSiteUri"></param>
        /// <param name="nName"></param>
        /// <returns></returns>
        internal string GetCookieValue(Uri nSiteUri, string nName)
        {
            Cookie cookie = _cookies.GetCookies(nSiteUri)[nName];
            return (cookie == null) ? null : cookie.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nRequest"></param>
        /// <returns></returns>
        internal HttpWebResponse GetResponse(HttpWebRequest nRequest)
        {
            if (nRequest == null)
            {
                throw new ArgumentNullException("request");
            }
            HttpWebResponse response = null;
            try
            {
                response = (HttpWebResponse)nRequest.GetResponse();
                _cookies.Add(response.Cookies);
                // Print the properties of each cookie.

                foreach (Cookie cook in _cookies.GetCookies(nRequest.RequestUri))
                {
                    Console.WriteLine("Domain: {0}, String: {1}", cook.Domain, cook.ToString());
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine("Web exception occurred. Status code: {0}", ex.Status);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return response;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nContent"></param>
        /// <param name="nMethod"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <returns></returns>
        internal HttpWebRequest GenerateRequest(string nUri, string nContent, string nMethod, string nLogin, string nPassword, bool nAllowAutoRedirect)
        {
            if (nUri == null)
            {
                throw new ArgumentNullException("uri");
            }
            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(nUri);
            // Set the Method property of the request to POST.
            request.Method = nMethod;
            // Set cookie container to maintain cookies
            request.CookieContainer = _cookies;
            request.AllowAutoRedirect = nAllowAutoRedirect;
            // If login is empty use defaul credentials
            if (string.IsNullOrEmpty(nLogin))
            {
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
            }
            else
            {
                request.Credentials = new NetworkCredential(nLogin, nPassword);
            }
            if (nMethod == "POST")
            {
                // Convert POST data to a byte array.
                byte[] byteArray = Encoding.UTF8.GetBytes(nContent);
                // Set the ContentType property of the WebRequest.
                request.ContentType = "text/html";
                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;
                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
            }
            return request;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <returns></returns>
        public HttpWebRequest GenerateGETRequest(string nUri, string nLogin, string nPassword, bool nAllowAutoRedirect)
        {
            return GenerateRequest(nUri, null, "GET", null, null, nAllowAutoRedirect);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nContent"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <returns></returns>
        public HttpWebRequest GeneratePOSTRequest(string nUri, string nContent, string nLogin, string nPassword, bool nAllowAutoRedirect)
        {
            return GenerateRequestappjson(nUri, nContent, "POST", null, null, nAllowAutoRedirect);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nContent"></param>
        /// <param name="nMethod"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <returns></returns>
        internal HttpWebRequest GenerateRequestappjson(string nUri, string nContent, string nMethod, string nLogin, string nPassword, bool nAllowAutoRedirect)
        {
            if (nUri == null)
            {
                throw new ArgumentNullException("uri");
            }
            // Create a request using a URL that can receive a post. 
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(nUri);
            // Set the Method property of the request to POST.
            request.Method = nMethod;
            // Set cookie container to maintain cookies
            request.CookieContainer = _cookies;
            request.AllowAutoRedirect = nAllowAutoRedirect;
            // If login is empty use defaul credentials
            if (string.IsNullOrEmpty(nLogin))
            {
                request.Credentials = CredentialCache.DefaultNetworkCredentials;
            }
            else
            {
                request.Credentials = new NetworkCredential(nLogin, nPassword);
            }
            if (nMethod == "POST")
            {
                // Convert POST data to a byte array.
                byte[] byteArray = Encoding.UTF8.GetBytes(nContent);
                // Set the ContentType property of the WebRequest.
                request.ContentType = "application/json";
                // Set the ContentLength property of the WebRequest.
                request.ContentLength = byteArray.Length;
                // Get the request stream.
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
            }
            return request;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nResponse"></param>
        /// <returns></returns>
        public string GetResponseContent(HttpWebResponse nResponse)
        {
            if (nResponse == null)
            {
                throw new ArgumentNullException("response");
            }
            Stream dataStream = null;
            StreamReader reader = null;
            string responseFromServer = null;

            try
            {
                // Get the stream containing content returned by the server.
                dataStream = nResponse.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                reader = new StreamReader(dataStream);
                // Read the content.
                responseFromServer = reader.ReadToEnd();
                // Cleanup the streams and the response.
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (dataStream != null)
                {
                    dataStream.Close();
                }
                nResponse.Close();
            }
            _lastResponse = responseFromServer;
            return responseFromServer;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nContent"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <returns></returns>
        public HttpWebResponse SendPOSTRequest(string nUri, string nContent, string nLogin, string nPassword, bool nAllowAutoRedirect)
        {
            HttpWebRequest request = GeneratePOSTRequest(nUri, nContent, nLogin, nPassword, nAllowAutoRedirect);
            return GetResponse(request);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <returns></returns>
        public HttpWebResponse SendGETRequest(string nUri, string nLogin, string nPassword, bool nAllowAutoRedirect)
        {
            HttpWebRequest request = GenerateGETRequest(nUri, nLogin, nPassword, nAllowAutoRedirect);
            return GetResponse(request);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nContent"></param>
        /// <param name="nMethod"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <returns></returns>
        public HttpWebResponse SendRequest(string nUri, string nContent, string nMethod, string nLogin, string nPassword, bool nAllowAutoRedirect)
        {
            HttpWebRequest request = GenerateRequest(nUri, nContent, nMethod, nLogin, nPassword, nAllowAutoRedirect);
            return GetResponse(request);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nContent"></param>
        /// <param name="nMethod"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <returns></returns>
        public HttpWebResponse SendRequestjson(string nUri, string nContent, string nMethod, string nLogin, string nPassword, bool nAllowAutoRedirect)
        {
            HttpWebRequest request = GenerateRequestappjson(nUri, nContent, nMethod, nLogin, nPassword, nAllowAutoRedirect);
            return GetResponse(request);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nUri"></param>
        /// <param name="nContent"></param>
        /// <param name="nMethod"></param>
        /// <param name="nLogin"></param>
        /// <param name="nPassword"></param>
        /// <param name="nAllowAutoRedirect"></param>
        /// <param name="nJson"></param>
        /// <returns></returns>
        public HttpWebResponse SendRequest(string nUri, string nContent, string nMethod, string nLogin, string nPassword, bool nAllowAutoRedirect, bool nJson)
        {
            HttpWebRequest request = GenerateRequestappjson(nUri, nContent, nMethod, nLogin, nPassword, nAllowAutoRedirect);
            return GetResponse(request);
        }

    }
}
