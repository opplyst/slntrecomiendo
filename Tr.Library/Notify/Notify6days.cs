﻿using System;
using Tr.Entities;
using Tr.DataAccess;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tr.Library
{
    public class Notify6Days : INotification
    {
        public void NotifyLaunch()
        {
            var userThreatment = GetUsers();

            foreach (var item in userThreatment)
            {
                Console.WriteLine($"Notificar usuarios 6 dias: {item.Nombre} , {item.CorreoElectronico}");

                NotifyAPI(item);
            }
        }


        /// <summary>
        /// Obtener Usuarios
        /// </summary>
        /// <returns></returns>
        private List<TblTrabajadore> GetUsers()
        {
            var DataAccessLayer = new Getter();
            return DataAccessLayer.GetUsersSixDays();
        }

        public void NotificarUsuario(TblTrabajadore nTrabajador)
        {
            var notificacion = new DataAccess.Setter() { };

            notificacion.DeleteTrabajador(nTrabajador);
        }
        
        public void NotifyAPI(TblTrabajadore nTrabajadores)
        {
            var url = "http://localhost:52730/API//Notify/UsersSix";


            //Transmitir a SAP
            string jsonResponseMensaje;
            var responseMensaje = SendRequest(url, nTrabajadores, out jsonResponseMensaje);

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            //NOTIFY TO
            Console.WriteLine("{0}", responseMensaje);



        }

        private static string SendRequest(string url, TblTrabajadore ResponseModel, out string jsonResponse)
        {
            var reqManager = new RequestManager();
            var retorno = String.Empty;

            var getMessage = reqManager.SendRequest(url, JsonConvert.SerializeObject(ResponseModel), "POST", null, null, false, true);
            jsonResponse = reqManager.GetResponseContent(getMessage);

            retorno += $"Mensaje Facturado text -{ResponseModel.Nombre}";

            return retorno;


        }

    }
}

